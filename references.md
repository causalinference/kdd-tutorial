# References

## Books
* Pearl, Judea. Causality. Cambridge university press, 2009.
* Morgan, Stephen L., and Christopher Winship. Counterfactuals and causal inference. Cambridge University Press, 2015.
* Imbens, Guido W., and Donald B. Rubin. Causal inference in statistics, social, and biomedical sciences. Cambridge University Press, 2015.
* Dunning, Thad. Natural experiments in the social sciences: a design-based approach. Cambridge University Press, 2012.
* Peters, Jonas, Dominik Janzing, and Bernhard Schölkopf. Elements of causal inference: foundations and learning algorithms. MIT Press, 2017.

## Papers
* Bottou, Léon, et al. "Counterfactual reasoning and learning systems: The example of computational advertising." The Journal of Machine Learning Research 14.1 (2013): 3207-3260.
* Eckles, Dean, and Eytan Bakshy. "Bias and high-dimensional adjustment in observational studies of peer effects." arXiv preprint arXiv:1706.04692 (2017).
* Olteanu, Alexandra, Onur Varol, and Emre Kiciman. "Distilling the outcomes of personal experiences: A propensity-scored analysis of social media." Proceedings of the 2017 ACM Conference on Computer Supported Cooperative Work and Social Computing. ACM, 2017.
* Shalizi, Cosma Rohilla, and Andrew C. Thomas. "Homophily and contagion are generically confounded in observational social network studies." Sociological methods & research 40.2 (2011): 211-239. 
* Sharma, Amit, Jake M. Hofman, and Duncan J. Watts. "Split-door criterion for causal identification: Automatic search for natural experiments." arXiv preprint arXiv:1611.09414 (2016).
* Wager, Stefan, and Susan Athey. "Estimation and inference of heterogeneous treatment effects using random forests." Journal of the American Statistical Association (2017). 


