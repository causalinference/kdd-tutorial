<iframe src="https://onedrive.live.com/embed?cid=FB9A18AE325D3EFB&amp;resid=FB9A18AE325D3EFB%215372&amp;authkey=APvzYaqRE4QHUN0&amp;em=2&amp;wdAr=1.7777777777777777" width="610px" height="367px" frameborder="0"></iframe>
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Special considerations when dealing with large-scale and network data
In this section of the tutorial, we focus on complexities that occur in causal studies using common kinds of datasets from online services.  We will provide examples of some well-designed studies and use them to review some of the more complex issues that arise.  While we will not be able to go into depth on solutions, we will teach attendees how to recognize these challenges and provide pointers to current research approaches.

The  contexts and challenges include:     
* Common confounders in social data analyses.         
* Interference due to network effects.         
* Challenges in high-dimensional data analyses.        
* Common issues when analyzing log data.     

